$fn=40; // defines the resolution for calculation by OpenSCAD

use <../pcbbox/box.scad>;
my_inside_width=184; // alias X
my_inside_height=45; // alias Z
my_inside_depth=86;  // alias Y
my_plexi=3.;
my_width=my_inside_width+2*my_plexi;
my_height=my_inside_height+2*my_plexi;
my_depth=my_inside_depth+2*my_plexi;
my_roof=30; // roof outside (for top and bottom)
my_full_width=my_width+2*my_roof;
my_full_depth=my_depth+2*my_roof;
// X et Y des coins des toits par rapport au repère relatif de box (qui a comme (0,0) le coin intérieur inférieur gauche
offset=-my_plexi-my_roof;
Xmin=offset;
Ymin=offset;
Xmax=Xmin+my_full_width;
Ymax=Ymin+my_full_depth;
echo("Xmin,Xmax : ",Xmin,Xmax," Ymin,Ymax : ",Ymin,Ymax);


box(
    width=my_width, // external width of the box
    height=my_height, // external height of the box (4 épaisseur plexi)
    depth=my_depth,  // external depth of the box
    thickness=my_plexi, // thickness of the wood (-0.05 for plexi)
    //fingerWidth=undef, // if undefine 2 times the thickness
    fingerWidth=6, // distance between squared holes for assembly
    extends=[my_roof,my_roof,0,0,0,0], // should top and bottom 'extend' in order to assemble the box without glue
    labelsSize=10,
    showLabels=false,
    labels=["Top","Bottom","Left","Right","Front","Back"],
    3d=false, // true: render as 3D, false: render as 2D (for laser cutting)
    space=2, // space between the parts when rendering as 2D
    active=[1,1,1,1,1,1],  // side that should be displayed
    holes=[ // relative to the inside down-left corner of the box
            // circulaire : [x,y,diam)
            // rectangle : [x,y,Dx,Dy]
        [   // Holes at the top
            [40,12,3], // (x,y,d) trou pour le reset
            [21+50/2,my_inside_depth-25-30/2,50,30], // aération des drivers moteurs
            [Xmin+15,-15,8], // trou G pour suspendre
            [Xmax-15,-15,8], // trou D pour suspendre
            [5,5,3.2], // trou GB entretoise
            [5,my_inside_depth-5,3.2],// trou GH entretoise
            [my_inside_width-5,5,3.2],// trou DB entretoise
            [my_inside_width-5,my_inside_depth-5,3.2] //trou DH entretoise
            
        ],
        [   // Holes on the bottom
            [30,76,3],    // trou à G en H pour RPi 
            [30,76-48,3], // trou à G en B pour RPi
            [30+58,76,3], // trou à D en H pour RPi
            [30+58,76-48,3], // trou àD en B pour RPi       
            [125+15,65,3],       // Trou pour distrib Tension
            [125+15+18,65-34,3], // Trou pour distrib Tension
            [Xmin+15,-15,5.5], // trou G pour suspendre
            [Xmax-15,-15,5.5], // trou D pour suspendre
            [5,5,3.2], // trou GB entretoise
            [5,my_inside_depth-5,3.2],// trou GH entretoise
            [my_inside_width-5,5,3.2],// trou DB entretoise
            [my_inside_width-5,my_inside_depth-5,3.2] //trou DH entretoise
        ],
        [   // Holes on the left
            [my_inside_depth-23-20/2,3+16/2,20,16] // rectangle pour cable réseau
        ],
        [   // Holes on the right
            //[40,15,35,2], // rectangle bas pour ventilation
            //[40,25,45,2], // rectangle haut pour ventilation
            // ATTENTION x et y doivent être inversés à cause d'un bug de la version du 1.11.2018
            [(my_inside_depth-32)/2,(my_inside_height-32)/2,3], 
            [(my_inside_depth-32)/2,(my_inside_height+32)/2,3], //32 de distance entre les trous des coins du ventilo
            [(my_inside_depth+32)/2,(my_inside_height-32)/2,3],
            [(my_inside_depth+32)/2,(my_inside_height+32)/2,3],
            [my_inside_depth/2,my_inside_height/2,18,my_inside_height-10],
            [my_inside_depth/2,my_inside_height/2,my_inside_height-10,18],
            [my_inside_depth/2,my_inside_height/2,32]
            
        ],
        [   // Holes on the front

        ],
        [   // Holes on the back
            [my_inside_width-33-36/2,my_inside_height-16-20/2,36,20], // trous pour les drivers moteurs
            
            [18,my_inside_height/2+4,20,12], // trou pour arrivée alim
            [42,my_inside_height/2+4,10,5],  // trou pour le mosfet
            [56,my_inside_height/2+4,6,5],  // trou pour le cable du mosfet à la gondole
        ]
    ]
);
