
module arc(){
    long=30;
    difference(){
        cylinder(d=15,h=long);
        cylinder(d=11,h=long);
    }
}
module pot(){
       difference(){
        cylinder(d=6,h=8);
        translate([0,0,2.2])cylinder(d=3.5,h=8,$fn=48);
    }
}
difference() {
    union(){
        rotate([90,0,0])translate([0,-3,0])arc();
        translate([0,-7.5,2.8])pot();
    }
    #translate([-7.5,-33,-12])cube([15,35,12]);
}
    