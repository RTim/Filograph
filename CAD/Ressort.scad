/* Serpent ressort
    Anneau g
    Anneua d
    
*/
$fn=48;
diam=8;
haut=10;
ep=diam/10;
sca=1.8;
nb=2;
module anneau(sens){
    difference(){
        cylinder(d=diam,h=haut);
        cylinder(d=diam-2*ep,h=haut);
        if(sens) {
            translate([0,-diam/2,0])cube([diam/2,diam,haut]);
        } else {
            translate([-diam/2,-diam/2,0])cube([diam/2,diam,haut]);
        }
    }
    
}
module serpent(){
 for (i = [0 : 2: nb])  {
translate([0,(diam - ep)*i,0])scale([sca,1,1])anneau(1);
translate([0,(diam - ep)*(i + 1),0])scale([sca,1,1])anneau(0);
     }
 }
 translate ([-diam,0,0]) serpent();
 translate ([2*diam,0,0]) serpent();
 translate ([5*diam,0,0]) serpent();
 
 translate([-2*diam,-diam/2,0])cube([8*diam,ep,haut]);
 translate([-2*diam,(nb+1)*(diam -ep) + diam/2 -ep,0])cube([8*diam,ep,haut]);