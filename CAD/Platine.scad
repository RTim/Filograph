/*
    Platine support des moteurs
    Semelle percée pour recevoir 4 vis de M3x30
*/
$fn=48;
xp=50;
ep=5;
zp=95;

// Nema 17
Ln17=31 ;
Hn17=42 ;
An17=24 ;
b17=10;

module platine(sens) {
    difference() {
        
     translate([0,0,0])cube([xp,ep,zp]);
     translate([5,0,zp-5]) rotate([-90,0,0]) cylinder(d=4,h=ep);
     translate([xp-5,0,zp-5]) rotate([-90,0,0]) cylinder(d=4,h=ep);
      if(sens==1){  
     rotate([0,-17,0])translate ([16.5,0,4])percements();
      } else {
     rotate([0,17,0])translate ([1.6,0,19])percements();
      }
     translate([xp/2,0,zp-40]) empreinte();
     translate([xp/2,0,zp-30]) fente();   
    }
}
//platine(1);
//translate([55,0,0])platine(0);
projection(cut=true)translate([0,0,-ep/4])rotate([90,0,0]) {
platine(1);
translate([55,0,0])platine(0);
}
//empreinte();
//fente();
//entretoise();
//rotate([0,-15,0])percements();
module empreinte(){
    translate([0,0,11])rotate([-90,0,0])cylinder(d=22,h=ep/2);
    translate([-11,0,10]) cube([22,ep/2,13]);
    translate([-7.5,0,0])cube([15,ep/2,22]);
    }
module fente() {
    translate([-1.5,0,23])cube([4.7,ep,20]);
    
}   
module percements(){
    translate([0,0,0])          
              rotate([-90,0,0]) cylinder(d=3,h=ep);
    translate([Ln17,0,0])          
              rotate([-90,0,0]) cylinder(d=3,h=ep);
    translate([0,0,Ln17])          
              rotate([-90,0,0]) cylinder(d=3,h=ep);
    translate([ Ln17,0,Ln17])          
              rotate([-90,0,0]) cylinder(d=3,h=ep);
} 

module entretoise(){
    difference(){
        cylinder(d=16,h=25);
        cylinder(d=3.5,h=25);
    }
}