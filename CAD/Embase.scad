$fn=64;

hext=5; // epaisseur disque

dint=25; // crayon diam gros marqueur
// Manchon
dm=30;
de=11; // Electro
hm=35;
angle=200; // pour l'axe des vis serrage et support courroie
//disque
dd=100;
// Boulon
hb=8;
db=13;
M8=8;
h8=20;
module manchon() {
difference() {
    union(){
    
    translate([0,0,0])cylinder(d=dm,h=hm); 
    rotate([0,0,angle])translate([0,-22,hext])courroie(); 
    rotate([0,0,-265])translate([-0,14,hext])oreille();
    rotate([0,0,305])translate([-1,14,hext])oreille(); 
    rotate([0,0,angle])translate([0,11,hext])electro();    
    }
    // evidement pour crayon
    cylinder(d=dint,h=hm,$fn=5);
    //fente
    rotate([0,0,360/5])translate([0,0,0])cube([dm/2,.7,hm]);
    rotate([90,0,angle])translate([0,25,0])cylinder(d=3.4,h=dm/2);
    #rotate([90,0,angle])translate([0,25,-2])cylinder(d=7,h=dm/2,$fn=6);
}
}
module courroie() {
    long=12;
    ep=6;
    haut=15;
    fente=4;
    diam=3;
    ecart=3;
    difference(){
        
        translate([-ep/2,0,0])cube([ep,long,haut]);
        translate([-ep/2-.7,0,0])cube([fente,long-2,haut]);
        translate([-ep/2,ecart,3])rotate([0,90,0])cylinder(d=3,h=ep);
        translate([-ep/2,ecart,12])rotate([0,90,0])cylinder(d=3,h=ep);
    }
}
module oreille(){
    ho=22;
    Lo=18;
    eo=6;
    difference(){
    translate([-eo/2,0,0])cube([eo,Lo,ho]);
    #translate([eo/2 +1,Lo/2,ho/2.3])rotate([0,-90,0])cylinder(d=8.5,h=eo+2);   
    }
}
//courroie();
color("red")manchon();
//color("ivory")rotate([0,0,18.5]) disque();
//oreille();
//projection() translate([0,0,hext/2]) disque();
//boulon();
module disque() {
    difference() {
        cylinder(d=dd,h=hext);
        rotate([0,0,15])translate([0,dd/2 -10,0]) cylinder(d=3,h=hext);
        rotate([0,0,-15])translate([0,dd/2 -10,0])cylinder(d=3,h=hext);
        cylinder(d=dm,h=hext);
        translate([0,-(dm/2+2.5+de/2),0]) cylinder(d=de,h=hext);
        rotate([0,0,40])translate([0,-dd/2 +h8,0])boulon();
        rotate([0,0,-40])translate([0,-dd/2 +h8,0])boulon();
    }
}
module electro() {
    long=19;
    ep=20;
    haut=30;
    fente=15.4;
    diam=3;
    ecart=10.5;
    dbas=11;
    dhaut=26;
    difference(){
        
        translate([-ep/2,0,0])cube([ep,long,haut]);
        #translate([-7.5,0,0])cube([fente,long,haut]);
        #translate([-ep/3,ecart,dbas])rotate([0,90,0])cylinder(d=3,h=ep);
        #translate([-ep/3,ecart,dhaut])rotate([0,90,0])cylinder(d=3,h=ep);
    }
}
module boulon(){
    translate([-db/2,-hb/2,0])cube([db,hb,hext]);
    
    translate([-M8/2,-h8-hb/2,0])cube([M8,h8,hext]);
    translate([-db/2-3,-h8-hb/2,0])cube([db+6,hb,hext]);
    
    
}