# Filograph

Polargraph revisité pour l'exposition Cosmos
FabLab Renens

Nous utilisons un RPi 3 équipé d'un shield Protoneer pour grbl.

Le grbl 1.1f n'est pas modifié. 
Nous transformons le G-code qui lui est destiné pour que les moteurs
X et Y agissent sur les courroies crantées GT2.( X ->A et Y -> B ).
Un électroaimant remplace le traditionnel servomoteur pour activer ou pas 
la trace du marqueur.

Nous voulons tenter l'autonomie de l'instrument à l'aide d'un RPi auquel
 nous envoyons les images à tracer par le réseau. La vectorisation et la transformation
 en coordonées "A B" peuvent raisonablement être effectuées sur ce 
 processeur. La gestion à bas niveau des actionneurs est courament confiée
 à un microcontroleur. Arduino est le candidat idéal pour cette tâche.
Ce circuit chargé du firmware grbl, contrôlant efficacement CNC et Lasercutter
 dans nos FabLabs, nous l'avons placé au coeur de cette architecture.
 
 Pour la partie logicielle nous ne prenons en charge que le G-Code fourni par l'outil gcodetools d'Inkscape.
 La vectorisation de n'importe quelle image n'est pas facilement automatisable et nous préférons la réaliser ailleurs.
 Le G-Code produit par Inkscape est alors envoyé sur un serveur web du RPi qui l'optimiise avec gocnc. 
 Ce G-Code destiné à une machine cartésienne est transformé pour le Filograph à l'aide d'un code écrit en Go.
 
 
 