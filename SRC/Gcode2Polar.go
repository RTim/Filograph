/*
 	Gcode4Polar.go 
	Entrée gcode coordonnées x y optimisé pour grbl
	github.com/joushou/gocnc 
	Génération d'un fichier gcode pour polargraph 
		- Géométrie de la machine x0 y0 d
		- Lire le fichier en entrée transformer x y en a b	
		- x -> a a=math.Hypot(x,y)
		- y -> b b=math.Hypot(d-x,y)
		- Transformer val de z en M8 z<0 M9 (z>0)
		- Coordonnées absolues ou relatives ?
		[ Doc sur Mosfet http://www.gammon.com.au/motors

*/
package main

import (
	"fmt"
	"io"
	"bufio"
	"github.com/akamensky/argparse"
	"math"
	"os"
	"strings"
	"strconv"
//	"errors"
	"unicode"
	)
	var Oldx , Oldy, Olda, Oldb, PrecX,PrecY float64
	var vala, valb, da, db, dx, dy float64
	var StatePen bool // scribe true
	var X0 float64
	var Y0 float64
	var D float64
	var Abs bool
	var Res float64
	

	//var Scale float64
	
func main() {

	parser := argparse.NewParser("Gcode2Polar", "Transforme gcode [x,y] en [a,b]")
	
	sx0 := parser.String("x", "X0",&argparse.Options{Required: true, Help: "Origine X0"})
    sy0 := parser.String("y", "Y0",&argparse.Options{Required: true, Help: "Origine Y0"})
    sd  := parser.String("d", "D",&argparse.Options{Required: true, Help: "Distance entre moteurs D"})
    ar := parser.Flag("a", "absolu",&argparse.Options{Help: "Coordonnées relatives ou absolues"}) 
    fs  := parser.String("o", "Output",&argparse.Options{Required: true, Help: "Fichier de sortie"})
    fe  := parser.String("i","Input", &argparse.Options{Required: true, Help: "Fichier d'entrée"})
	err := parser.Parse(os.Args)
	if err != nil {
		// In case of error print error and print usage
		// This can also be done by passing -h or --help flags
		fmt.Print(parser.Usage(err))
	}
X0, err = strconv.ParseFloat(*sx0, 64)
			if err != nil {
				fmt.Println("Erreur",err)		
			}
Y0, err = strconv.ParseFloat(*sy0, 64)
			if err != nil {
				fmt.Println("Erreur",err)		
			}
D, err = strconv.ParseFloat(*sd, 64)
			if err != nil {
				fmt.Println("Erreur",err)		
			}

			
Olda=math.Hypot(X0,Y0)
Oldb=math.Hypot(D-X0,Y0)

Oldx=0.0
Oldy=0.0
	
StatePen=false
		
/* Lecture fichier gcode  
   optimisé pour grbl par github.com/joushou/gocnc
   En dehors de l'optimisation, le fait que les arcs soient transformés
   en segments facilite le traitement de transformation
*/
	Abs=*ar
	//if D < 2000 { Res = 10.0} else {Res = 30.0}
file,err := os.Open(*fe)
    if err != nil {
        panic(err)
    }
Sor,err := os.Create(*fs)
    if err != nil {
        panic(err)
	}
s := bufio.NewScanner(file)
	
	// read line by line
	for s.Scan() {
		// parse line
		ligne := string(s.Text())
		if ligne != "" && ligne[0]!= '(' {	// On laisse les commentaires		
			i := strings.LastIndexFunc(ligne, unicode.IsUpper)
			if i > 0 {
				j := strings.LastIndexFunc(ligne[0:i], unicode.IsUpper)
				if j > 0 {
					ParseLigne(ligne[0:j],false,Sor)
					ParseLigne(ligne[j:i],false,Sor)
					ParseLigne(ligne[i:],true,Sor)
				} else {
					ParseLigne(ligne[0:i],false,Sor)
					ParseLigne(ligne[i:],true,Sor)
				}				
			} else {
				ParseLigne(ligne,true,Sor)
			}			
		}
	}
}

func ParseLigne (s string, fl bool,sor io.Writer){
	if s[0] == 'G'{
		// parse value, always a value after a letter
		f, err := strconv.ParseFloat(s[1:], 64)
			if err != nil {
				fmt.Println("Erreur",s,err)
				panic(err)
			}
		if int(f) == 90 && ! Abs {
			 fmt.Fprintf (sor,"%s","G91")
		} else {
			 fmt.Fprintf (sor,"%s",s)			 
		}
		
			
	} else if s[0] == 'F'{ 
		fmt.Fprintf (sor,"%s\n",s)
	} else if s[0] == 'M'{
		if s[1] == '8' || s[1] == '9' {
			fmt.Fprintf (sor,"%s\n",s)
		} else {
			if StatePen == false {
				fmt.Fprintf (sor,"\n%s\n","M8")
			} else {
				fmt.Fprintf (sor,"\n%s\n","M9")
			}
		}
	} else if s[0] == 'Z'{	
	f, err := strconv.ParseFloat(s[1:], 64)
			if err != nil {
				fmt.Println("Erreur",s,err)
				panic(err)				
			}
		if f > 0 {
			 StatePen= false
			 fmt.Fprintf (sor,"\n%s\n","M8")
		} else {
			 StatePen=true
			 fmt.Fprintf (sor,"\n%s\n","M9")
		}
	} else if s[0] == 'X'{
	f, err := strconv.ParseFloat(s[1:], 64)
			if err != nil {
				fmt.Println("Erreur",s,err)
				panic(err)
			}
		if  !fl {	// on attend Y
			Oldx=f
		} else {
			// pas de nouvelle valeur de y on calcule a et b
			if (math.Abs(f-PrecX) > 20 && StatePen == true ){
				//fmt.Println("\nX=",f," PrecX=",PrecX)
				Res=math.Floor(math.Abs(f-PrecX)/5)
				//fmt.Println("Res =",Res)
				for  i:=1;i<int(Res)+1;i++ {
					if PrecX > f {
						if PrecX > 0 && f >= 0 { dx=PrecX - float64(i)*(PrecX -f)/Res
						} else if PrecX >= 0 && f <= 0 { dx=PrecX - float64(i)*(PrecX -f)/Res
						} else if PrecX <= 0 && f < 0 { dx=PrecX - float64(i)*(PrecX -f)/Res}
					} else {
						if PrecX >= 0  {dx=PrecX + float64(i)*(f - PrecX)/Res
					    } else if f > 0 {dx=PrecX + float64(i)*(f - PrecX)/Res
						} else {dx=PrecX + float64(i)*(f - PrecX)/Res}
					}
					//fmt.Println("dx=",dx)
					vala = math.Hypot(dx + X0,Oldy + Y0)
					valb = math.Hypot(D - dx - X0,Oldy + Y0)
					da = vala - Olda
					db = valb - Oldb
					Olda = vala
					Oldb = valb
					if Abs {
						fmt.Fprintf(sor,"X%fY%f\n",vala,valb)
					} else {
						fmt.Fprintf(sor,"X%fY%f\n",da,db)
					}
				}
			}else{
				vala = math.Hypot(f + X0,Oldy + Y0)
				valb = math.Hypot(D - f - X0,Oldy + Y0)
				da = vala - Olda
				db = valb - Oldb
				Olda = vala
				Oldb = valb
				if Abs {
					fmt.Fprintf(sor,"X%fY%f\n",vala,valb)
				} else {
					fmt.Fprintf(sor,"X%fY%f\n",da,db)
				}
			}
			Oldx=f
			PrecX=f
		}
		
	} else if s[0] == 'Y'{
	f, err := strconv.ParseFloat(s[1:], 64)
			if err != nil {
				fmt.Println("Erreur",s,err)
				panic(err)
			}
		if  !fl {
			fmt.Println("Erreur position ?",s)
		} else {
			if (math.Abs(Oldx - PrecX) > 20 || math.Abs(f - PrecY) > 20) && StatePen == true {
				//fmt.Println("\nX=",Oldx," PrecX=",PrecX," Y= ",f," PrecY=",PrecY)
					Res=math.Floor(math.Max(math.Abs(Oldx-PrecX)/5,math.Abs(f - PrecY)/5))
					//fmt.Println("Res =",Res)
				for  i:=1;i<int(Res)+1;i++ {
					if PrecX > Oldx {
						if PrecX > 0 && Oldx >= 0 { dx=PrecX - float64(i)*(PrecX -Oldx)/Res 
						} else if PrecX >= 0 && Oldx < 0 { dx=PrecX - float64(i)*(PrecX -Oldx)/Res
						} else if PrecX < 0 && Oldx < 0 { dx=PrecX - float64(i)*(PrecX -Oldx)/Res}
					} else {
						if PrecX >= 0  {dx=PrecX + float64(i)*(Oldx - PrecX)/Res
					    } else if Oldx >= 0 {dx=PrecX + float64(i)*(Oldx -PrecX)/Res
						} else {dx=PrecX + float64(i)*(Oldx -PrecX)/Res}
					}
					if PrecY > f {
						if PrecY >= 0 && f >= 0 { dy=PrecY - float64(i)*(PrecY -f)/Res 
						} else if PrecY >= 0 && f <= 0 { dy=PrecY - float64(i)*(PrecY -f)/Res
						} else if PrecY <= 0 && f <= 0 { dy=PrecY - float64(i)*(PrecY -f)/Res}
					} else {
						if PrecY >= 0  {dy=PrecY + float64(i)*(f - PrecY)/Res
					    } else if f >= 0 {dy=PrecY + float64(i)*(f -PrecY)/Res
						} else {dy=PrecY + float64(i)*(f -PrecY)/Res}
					}
					//fmt.Println("dx=",dx," dy=",dy)
					vala = math.Hypot(dx + X0,Y0 - dy)
					valb = math.Hypot(D - dx -X0,Y0 - dy)
			
					da = vala - Olda
					db = valb - Oldb
					Olda = vala
					Oldb = valb
					if Abs {
						fmt.Fprintf(sor,"X%fY%f\n",vala,valb)
					} else {
						fmt.Fprintf(sor,"X%fY%f\n",da,db)
					}
					
				}
			}else{
			vala = math.Hypot(Oldx + X0,Y0 - f)
			valb = math.Hypot(D - Oldx -X0,Y0  - f)
			
			da = vala - Olda
			db = valb - Oldb
			Olda = vala
			Oldb = valb
				if Abs {
					fmt.Fprintf(sor,"X%fY%f\n",vala,valb)
				} else {
					fmt.Fprintf(sor,"X%fY%f\n",da,db)
				}
			}
			Oldy= -f
			PrecY=-Oldy
			PrecX=Oldx
			
		}
	} else {
		fmt.Println("Erreur ?",s)
	}
}

