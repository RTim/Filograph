#!/bin/bash

#
# Launch script for filograph web front-end
#
# NOTE : In order to be launch on startup by crontab @reboot,
# GoCNC related environment variables must be initialized 
# on the top of your .bashrc file !
#

# Make it accessible from port 80 without super-user rights
sudo iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 5000

# Move to current script directory
cd $(dirname ${BASH_SOURCE[0]})

# Launch webserver
python filograph.py

exit 0
