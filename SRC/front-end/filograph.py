#!/usr/bin/env python
# -*- coding: utf-8 -*-
import flask
import os
import subprocess
from flask import Flask, flash, request, redirect, url_for, render_template
from werkzeug.utils import secure_filename

# https://mortoray.com/2014/03/04/http-streaming-of-command-output-in-python-flask/
# https://www.tutorialspoint.com/flask/flask_quick_guide.htm
# http://flask.pocoo.org/docs/1.0/patterns/fileuploads/

# -- OPTIONS --------------------------------------------------------------------------
STREAM_TOOL = '../stream.py' # ADAPT TO YOUR DEPLOYMENT
DEVICE = "/dev/ttyAMA0"
UPLOAD_FOLDER = 'uploads'
ALLOWED_EXTENSIONS = set(['ngc'])
# ----------------------------------------------------------------------------

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

app = flask.Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

# ----------------------------------------------------------------------------
app.secret_key = b'App sercret to change !'
# ----------------------------------------------------------------------------

# Tool method to get target downloaded file
def get_fullpath(filename):
    return os.path.join(app.config['UPLOAD_FOLDER'], filename)

# Handle of current processed command
handle = None

# Log file handle
log = open(get_fullpath("console.log"), 'w')

# Tool method used for terminate current processed command
def kill():
    global handle
    if handle:
        handle.terminate()
        handle.kill()
        handle.wait()
        handle = None 

# Allowed files while list accessor
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

# Render default route to upload page
@app.route('/')
def home():
    return render_template('upload.html')

# Request for starting a print job
@app.route('/start/<string:filename>')
def start(filename):
    cmd([ "python", "-u", STREAM_TOOL, "{}_final".format(get_fullpath(filename)), DEVICE ])
    return redirect(url_for('run', filename=filename))

# Page view during print
@app.route('/run/<string:filename>')
def run(filename):
    return render_template('run.html', filename=filename)

# Page request to stop current print
@app.route('/stop/<string:filename>')
def stop(filename):
    kill()
    return render_template('process.html', filename=filename)

# Page request for file upload
@app.route('/upload', methods=['POST'])
def upload():
    # check if the post request has the file part 
    if 'file' not in request.files:
        flash('No file part')
        return redirect(url_for('home'))
    file = request.files['file']
    # if user does not select file, browser also
    # submit an empty part without filename
    if file.filename == '':
        flash('No selected file')
        return redirect(url_for('home'))
    if not file or not allowed_file(file.filename):
        flash('Invalid file')
        return redirect(url_for('home'))
    filename = secure_filename(file.filename)
    file.save(get_fullpath(filename))
    return redirect(url_for('process', filename=filename, x=request.form['x'], y=request.form['y'], d=request.form['d']))

# Page request to process uploaded content and make compatible to print area
@app.route('/process/<string:filename>/<int:x>/<int:y>/<int:d>')
def process(filename, x, y, d):
    filename = secure_filename(filename)
    cmd([ "sbin/process.sh", get_fullpath(filename), str(x), str(y), str(d) ])
    return render_template('process.html', filename=filename)

# Run shell command
def cmd(cmd):
    global handle
    global log

    kill()

    handle = subprocess.Popen(
        cmd,
        bufsize=0,
        stdout=log,
        stderr=subprocess.STDOUT,
        stdin=subprocess.PIPE
    )

# Retrieve logs periodically
@app.route( '/api/stream' )
def stream():
    return flask.Response(subprocess.check_output(["tail", "-n", "20", get_fullpath("console.log")]), mimetype='text/plain')

# Launch app
if __name__ == "__main__":
    app.run(host='0.0.0.0')

