#!/bin/bash

filename=$1
x=$2
y=$3
d=$4
s=$5

echo "Filename=$filename x=$x y=$y d=$d vitesse=$s"

echo "Optimization for curves..."
/home/pi/.gvm/pkgsets/go1.10.2/global/bin/gocnc -o ${filename}_optimized $filename
/usr/bin/perl -i -pe "s/F400/F$s/g" ${filename}_optimized
echo "Coordinate conversion and solenoid management..."
../Gcode2Polar -i ${filename}_optimized -o ${filename}_final -x $x -y $y -d $d

echo "Done."

exit 0
